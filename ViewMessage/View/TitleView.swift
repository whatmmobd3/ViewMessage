import SwiftUI


struct TitleView: View {
    
    @EnvironmentObject var appData: AppData
    @State var itemList = [423]

    let message: MessageModel
    
    var body: some View {
        
        VStack(alignment: .leading) {
       
            Text(message.title )
                .font(.title)
            
            Button(action: {
     
                if itemList.contains(message.id){
                    itemList = itemList.filter(){$0 != message.id}
                }else{
                    itemList.append(message.id)
                }
                
                UserDefaults.standard.set(itemList,forKey: "myKey")

            }, label: {
                Image(systemName: "heart.circle")
                    .font(.largeTitle)
            })
            
                .foregroundColor(itemList.contains(message.id) ? .pink : .gray )
        }
        .border(Color.gray)
    }
}

struct TitleView_Previews: PreviewProvider {
    static var previews: some View {
        TitleView(message: messages[1])
            .previewLayout(.fixed(width: 200, height: 300))
            .environmentObject(AppData())
        
    }
}




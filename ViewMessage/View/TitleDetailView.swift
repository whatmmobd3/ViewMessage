import SwiftUI


struct TitleDetailView: View {
    @EnvironmentObject var appData: AppData
    
    let jsonURL = "https://cdn.home-designing.com/wp-content/uploads/2019/10/modern-fireplace-1.jpg"
    
    var body: some View {
        
        VStack(alignment: .leading, spacing: 10.0) {
            Button(action: {
                appData.showMessage = false
                appData.selectedMessage = nil
            }, label: {
                Image(systemName: "chevron.left")
                    .font(.title)
                    .foregroundColor(.blue)
            })
            Text("Title")
                .font(.title)
                .fontWeight(.bold)
            Text(appData.selectedMessage?.title ?? sampleMessage.title)
            
            Text("Content")
                .font(.title)
                .fontWeight(.bold)
            Text(appData.selectedMessage?.content ?? sampleMessage.content)

            Text("List of Images")
                .font(.title)
                .fontWeight(.bold)

            RemoteImage(url: jsonURL)
                .aspectRatio(contentMode: .fit)
                            .frame(width: 200)
                    
            Text("Number of comments: \(appData.selectedMessage?.totalComments ?? sampleMessage.totalComments)")
                .font(.title)
                .fontWeight(.bold)
            Spacer()

        }
        .padding(.leading,20)
    }
}

struct TitleDetailView_Previews: PreviewProvider {
    static var previews: some View {
        TitleDetailView()
            .environmentObject(AppData())
    }
}


//Title
//Content
//List of images
//Number of comments

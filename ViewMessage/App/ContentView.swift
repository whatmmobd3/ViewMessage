import SwiftUI

struct ContentView: View {
    @EnvironmentObject var appData: AppData
    
    var body: some View {
        VStack {
            if appData.showMessage == false && appData.selectedMessage == nil {
                VStack {
                    Text("All Messages")
                        .font(.largeTitle)
                        .foregroundColor(.blue)
                    ScrollView(){
                        VStack(alignment: .leading, spacing: 5.0) {
                            ForEach(messages){
                                msg in
                                TitleView(message: msg)
                                    .onTapGesture{
                                        appData.showMessage = true
                                        appData.selectedMessage = msg
                                    }
                            }
                            .padding(.leading,10)
                        }
                    }
                }
            } else {
                TitleDetailView()
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
            .environmentObject(AppData())
    }
}

//
//  ViewMessageApp.swift
//  ViewMessage
//
//  Created by Loc Nguyen on 12/10/2021.
//

import SwiftUI

@main
struct ViewMessageApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
                .environmentObject(AppData())
        }
    }
}

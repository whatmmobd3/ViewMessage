import Foundation

struct MessageModel: Codable, Identifiable{
    let id: Int
    let title: String
    let content: String
    var attachmentList: [Obj]
    let totalComments: Int

}

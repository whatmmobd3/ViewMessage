import Foundation


class AppData: ObservableObject{
    @Published var showMessage: Bool = false
    @Published var selectedMessage: MessageModel?
}
